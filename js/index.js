var acceuil =  document.querySelector(".acceuil span")
var portfolio =  document.querySelector(".portfolio span")
var contact =  document.querySelector(".contact span")

let fontcolor = "white"
let backgroundcolor = "black"

var wheel = document.getElementById("wheel")
window.addEventListener('scroll', function(e) {
    wheel.style.transform = "rotate("+document.scrollingElement.scrollTop/4+"deg)";
    acceuil.style.left = -document.scrollingElement.scrollTop * 1 + 'px';
    portfolio.style.left = -document.scrollingElement.scrollTop * 1 + 'px';
    contact.style.left = -document.scrollingElement.scrollTop * 1 + 'px';


});
var color = document.getElementById("color")
var back = false
color.addEventListener('click', function (e) {
    if (back) {
        back = false;
        fontcolor = "white";
        backgroundcolor = "black";

    }
    else {
        back = true;
        fontcolor = "black";
        backgroundcolor = "white";
    }
    document.body.style.cssText = "color :"+fontcolor + "!important";
    document.body.style.backgroundImage = "url(\"img/back_"+backgroundcolor+".png\")";
    wheel.src = "img/notre_fonctionnement_"+backgroundcolor+".png";

    document.querySelectorAll("svg line").forEach(e => e.style.stroke = fontcolor);
    document.querySelectorAll(".f").forEach(e => {
        e.style.borderBottomColor = fontcolor;
        e.style.color = fontcolor;
        if (back) e.classList.add("ph_black"); else e.classList.remove("ph_black");
        }
    );
    document.querySelector(".g-recaptcha").style.borderColor = fontcolor;
    document.querySelector(".g-recaptcha").style.color = fontcolor;

    color.style.backgroundColor = fontcolor;

    document.querySelectorAll('#burger span').forEach(e => e.style.background = fontcolor);
})



//annimations


//Fade in bottom
const fadeInBottom = new IntersectionObserver(entries => {
    entries.forEach(entry=> {
        if (entry.isIntersecting) {
            entry.target.classList.add("fadeInBottom");
            return
        }
    });
});
var classToAnimate = document.querySelectorAll('.in');
classToAnimate.forEach(element => fadeInBottom.observe(element));


//Fade in left
const fadeInLeft = new IntersectionObserver(entries => {
    entries.forEach(entry=> {
        if (entry.isIntersecting) {
            entry.target.classList.add("fadeInLeft");
            return
        }
    });
});
var classToAnimate2 = document.querySelectorAll('  #nomsoc > div, #nomsoc > svg, #tel-mail');
classToAnimate2.forEach(element => fadeInLeft.observe(element));



//photos sites
var list = document.getElementById("nomsoc").querySelectorAll('h1');
var active = document.getElementById("precym");
var new_img = document.getElementById("img_display");
active.style.color = "transparent";
list.forEach(element =>
    element.addEventListener("click", function (e) {
        active.style.color = null;
        element.style.color = "transparent";
        active = document.getElementById(element.id);
        new_img.src = "img/"+element.id+".png";
        new_img.classList.add("fadeInright");
        setTimeout(function(){
            new_img.classList.remove("fadeInright");
        },1000);
    } )

);


//scrolling
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

//burger
var burger = true;
document.getElementById('burger').addEventListener('click', function (e) {
    console.log("test");
    if (burger) {
        document.getElementById('burger').classList.add("open");
        document.getElementById('menu').style.display = 'flex';
        burger = false;
    }
    else {

        document.getElementById('burger').classList.remove("open");
        document.getElementById('menu').style.display = 'none';
        burger = true;
    }
})

/*$(document).ready(function(){
    $('#burger').click(function(){
        $(this).toggleClass('open');
        $('#title').style.marginTop = '5%';
        $('#menu').css("display", "flex");
    });
});*/

//form contact
const form = document.querySelector("form");
function captchaSubmit(data) {
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "serv/mail.php", true);
    let formData = new FormData(form);
    xhr.send(formData);
    xhr.onload = ()=>{
        if(xhr.readyState === 4 && xhr.status == 200){
            let response = xhr.response;
            if(response.indexOf("field_required") !== -1 || response.indexOf("valid") !== -1 || response.indexOf("failed") !== -1){
            }
            else{
                form.reset();
                setTimeout(()=>{
                    form.reset();
                }, 3000);
            }
        }
    }

}